import React from 'react';
import { Text, Button, View } from 'react-native';

export const HomeScreen = ({ navigation }) => {
    return (
        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
            <View style={{ width: 200 }}>
                <Button
                    color="#f194ff"
                    title="See react readme page"
                    onPress={() =>
                        navigation.navigate('Profile', { name: 'Jane' })
                    }
                />

                <Button
                    color="#f194ff"
                    title="Open barcode reader"
                    onPress={() =>
                        navigation.navigate('BarCodeReader')
                    }
                />

            </View>
        </View>
    );
};